#include "types.h"
#include "stat.h"
#include "fcntl.h"
#include "user.h"
#include "x86.h"

#define PGSIZE (4096)

int 
thread_create(void (*start_routine)(void*), void *arg)
{
	lock_t L;
	lock_init(&L);

	lock_acquire(&L);
	void *stack = malloc(4096);
   	if((uint)stack % PGSIZE)
     	stack = stack + (PGSIZE - (uint)stack % PGSIZE);
	lock_release(&L);
	return clone(start_routine, arg, stack);
}

int
thread_join()
{
  void* stack;
  int pid;
  if((pid = join(&stack)) == -1)
    return -1;
  else
  {
    free(stack);
    return pid;
  }
}
